#################################################################
#								#
# Copyright (c) 2019 YottaDB LLC and/or its subsidiaries.	#
# All rights reserved.						#
#								#
#	This source code contains the intellectual property	#
#	of its copyright holder(s), and is made available	#
#	under a license.  If you do not know the terms of	#
#	the license, please stop and do not read further.	#
#								#
#################################################################

load test_helpers

setup() {
  init_test
  createdb
  load_fixture names.sql
  load_fixture names.zwr
}

@test "TNJ00 : natural join no matches" {
  # If there are no matches in a natural join, it is effectively a cross join
  octo <<OCTO 2>&1 | tee output.txt
SELECT *
FROM (SELECT id as A FROM names) n1
NATURAL JOIN (SELECT id as B FROM names) n2;
OCTO
  verify_output TNJ00 output.txt
}

@test "TNJ01 : natural join many matches" {
  octo <<OCTO 2>&1 | tee output.txt
SELECT *
FROM names n1
NATURAL JOIN names n2;
OCTO
  verify_output TNJ01 output.txt
}

@test "TNJ02 : natural join many matches with many tables" {
  octo <<OCTO 2>&1 | tee output.txt
SELECT *
FROM names n1
NATURAL JOIN names n2
NATURAL JOIN names n3
NATURAL JOIN names n4;
OCTO
  verify_output TNJ02 output.txt
}

@test "TNJ03 : natural join with no alias on right table" {
  octo <<OCTO 2>&1 | tee output.txt
SELECT *
FROM names n1
NATURAL JOIN names;
OCTO
  verify_output TNJ03 output.txt
}

@test "TNJ04 : natural join with right alias longer then the left" {
  octo <<OCTO 2>&1 | tee output.txt
SELECT *
FROM names n1
NATURAL JOIN names as longeralias;
OCTO
  verify_output TNJ04 output.txt
}

