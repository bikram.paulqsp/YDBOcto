#################################################################
#								#
# Copyright (c) 2019 YottaDB LLC and/or its subsidiaries.	#
# All rights reserved.						#
#								#
#	This source code contains the intellectual property	#
#	of its copyright holder(s), and is made available	#
#	under a license.  If you do not know the terms of	#
#	the license, please stop and do not read further.	#
#								#
#################################################################

load test_helpers

setup() {
  init_test
  createdb
  load_fixture pastas.sql
  load_fixture pastas.zwr
  load_fixture names.sql
  load_fixture names.zwr
}

@test "long query line: inner join pastas order by pasta id ascending" {
  octo <<OCTO &> output.txt
select pastas.id, firstName, lastName, pastaName from names4 inner join pastas on pastas.pastaName = names4.favoritePasta order by pastas.id;
OCTO
  verify_output TL000 output.txt
}

@test "long query line: inner join pastas order by pasta id ascending explicit" {
  octo <<OCTO &> output.txt
select pastas.id, firstName, lastName, pastaName from names4 inner join pastas on pastas.pastaName = names4.favoritePasta order by pastas.id asc;
OCTO
  verify_output TL001 output.txt
}

@test "long query line: inner join pastas order by pasta id descending" {
  octo <<OCTO &> output.txt
select pastas.id, firstName, lastName, pastaName from names4 inner join pastas on pastas.pastaName = names4.favoritePasta order by pastas.id desc;
OCTO
  verify_output TL002 output.txt
}

@test "octo -f on query greater than 32k characters in one query" {
  export COLUMNS=35000 # increase column limit to avoid control characters
  yottadb -run gt32k^genlargequery
  octo -f in.sql &> output.txt
  # filter out large column and error messages
  # these are exact counts so any change in output will show in outref
  sed -e 's/a\(1234567890\)\{1638\}1234/COLUMN_A/ig'\
    -e 's/b\(1234567890\)\{1638\}1234/COLUMN_B/ig'\
    -e 's/^\^\{16385\}/COLUMN_A_ERROR/g'\
    -e 's/^ \^\{16385\}/COLUMN_B_ERROR/g' output.txt > filteredOut.txt
  verify_output TL003 filteredOut.txt
}

@test "octo -f on series of queries greater than 32k characters" {
  export COLUMNS=35000 # increase column limit to avoid control characters
  yottadb -run almost32k^genlargequery
  octo -f in.sql &> output.txt
  # filter out large column and error messages
  # these are exact counts so any change in output will show in outref
  sed -e 's/a\(1234567890\)\{1637\}/COLUMN_A/ig'\
    -e 's/b\(1234567890\)\{1637\}/COLUMN_B/ig'\
    -e 's/^\^\{16371\}/COLUMN_A_ERROR/g'\
    -e 's/^ \^\{16371\}/COLUMN_B_ERROR/g' output.txt > filteredOut.txt
  verify_output TL004 filteredOut.txt
}

@test "octo query greater than 32k characters in one query" {
  # This test fails on CentOS due to line limit
  if [[ $( grep -i "centos" /etc/*-release ) ]]; then
    skip "Skipping test as it does not work on CentOS"
  fi
  export COLUMNS=35000 # increase column limit to avoid control characters
  yottadb -run gt32k^genlargequery
  octo < in.sql &> output.txt
  # filter out large column and error messages
  # these are exact counts so any change in output will show in outref
  sed -e 's/a\(1234567890\)\{1638\}1234$/COLUMN_A/ig'\
    -e 's/b\(1234567890\)\{1638\}1234$/COLUMN_B/ig'\
    -e 's/^\^\{16385\}$/COLUMN_A_ERROR/g'\
    -e 's/^ \^\{16385\}$/COLUMN_B_ERROR/g' output.txt > filteredOut.txt
  verify_output TL005 filteredOut.txt
}

@test "octo series of queries greater than 32k characters" {
  # This test fails on CentOS due to line limit
  if [[ $( grep -i "centos" /etc/*-release ) ]]; then
    skip "Skipping test as it does not work on CentOS"
  fi
  export COLUMNS=35000 # increase column limit to avoid control characters
  yottadb -run almost32k^genlargequery
  octo < in.sql &> output.txt
  # filter out large column and error messages
  # these are exact counts so any change in output will show in outref
  sed -e 's/a\(1234567890\)\{1637\}$/COLUMN_A/ig'\
    -e 's/b\(1234567890\)\{1637\}$/COLUMN_B/ig'\
    -e 's/^\^\{16371\}$/COLUMN_A_ERROR/g'\
    -e 's/^ \^\{16371\}$/COLUMN_B_ERROR/g' output.txt > filteredOut.txt
  verify_output TL006 filteredOut.txt
}

@test "octo -f on query that overlaps with itself when shifting the input buffer" {
  # This test fails on CentOS due to line limit
  if [[ $( grep -i "centos" /etc/*-release ) ]]; then
    skip "Skipping test as it does not work on CentOS"
  fi
  export COLUMNS=35000 # increase column limit to avoid control characters
  yottadb -run bufoverlap^genlargequery
  octo -f in.sql &> output.txt
  # filter out large column and error messages
  # these are exact counts so any change in output will show in outref
  sed -e 's/a\(1234567890\)\{500\}$/COLUMN_A1/ig'\
    -e 's/b\(1234567890\)\{500\}$/COLUMN_B1/ig'\
    -e 's/^\^\{5001\}$/COLUMN_A1_ERROR/g'\
    -e 's/^ \^\{5001\}$/COLUMN_B1_ERROR/g'\
    -e 's/a\(1234567890\)\{1638\}1234$/COLUMN_A2/ig'\
    -e 's/b\(1234567890\)\{1638\}1234$/COLUMN_B2/ig'\
    -e 's/^\^\{16385\}$/COLUMN_A2_ERROR/g'\
    -e 's/^ \^\{16385\}$/COLUMN_B2_ERROR/g' output.txt > filteredOut.txt
  verify_output TL007 filteredOut.txt
}

@test "octo on query that overlaps with itself when shifting the input buffer" {
  # This test fails on CentOS due to line limit
  if [[ $( grep -i "centos" /etc/*-release ) ]]; then
    skip "Skipping test as it does not work on CentOS"
  fi
  export COLUMNS=35000 # increase column limit to avoid control characters
  yottadb -run bufoverlap^genlargequery
  octo < in.sql &> output.txt
  # filter out large column and error messages
  # these are exact counts so any change in output will show in outref
  sed -e 's/a\(1234567890\)\{500\}$/COLUMN_A1/ig'\
    -e 's/b\(1234567890\)\{500\}$/COLUMN_B1/ig'\
    -e 's/^\^\{5001\}$/COLUMN_A1_ERROR/g'\
    -e 's/^ \^\{5001\}$/COLUMN_B1_ERROR/g'\
    -e 's/a\(1234567890\)\{1638\}1234$/COLUMN_A2/ig'\
    -e 's/b\(1234567890\)\{1638\}1234$/COLUMN_B2/ig'\
    -e 's/^\^\{16385\}$/COLUMN_A2_ERROR/g'\
    -e 's/^ \^\{16385\}$/COLUMN_B2_ERROR/g' output.txt > filteredOut.txt
  verify_output TL008 filteredOut.txt
}
